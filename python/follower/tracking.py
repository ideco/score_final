from ChromaFeatures import ChromaFeatures
from ChromaFeatures import ChromaFactory
from OnlineAlignment import OnlineAlignment
from Convert import Convert
from Evaluation import Evaluation
from PlotResults import *
from Interpolation import *
import numpy as np
import cPickle as pickle

csvFile = "../metadata/mappings_interpolation.csv"
sampleFile = '../samples/noc1-0-30.wav'
referenceFile = "../samples/noc2-0-30.wav"
mappingFile = "../metadata/mappings.csv"
resultFile = "../metadata/diffs.p"

frameLength = 4096*4
penalty = 8
reward = 1
baseScore = 3

sampleLength = 2  # seconds

# Create service instances
convert = Convert()
evaluation = Evaluation(mappingFile, resultFile)

#evaluation = Evaluation(mappingFile, resultFile)
chromaFactory = ChromaFactory()

#A = np.random.random((3,5))

sample = chromaFactory.getChromaFeatures(sampleFile).getFeatures(frameLength)
reference = chromaFactory.getChromaFeatures(referenceFile).getFeatures(frameLength)

# TODO: At the moment we are losing frames with each conversion
allDiffs = []
for mapNr, mapping in enumerate(evaluation.mappings):
	print "Mapping: " + str(mapNr)

	# Retrieve file names
	sampleFile = mapping["sampleFile"]
	referenceFile = mapping["referenceFile"]
	# Retrieve mapped times
	sampleTimes = (mapping["sampleStart"], mapping["sampleEnd"])
	expectedTimes = (mapping["expectedStart"], mapping["expectedEnd"])

	# Get Chroma Features
	sample = chromaFactory.getChromaFeatures(sampleFile).getFeatures(frameLength)
	reference = chromaFactory.getChromaFeatures(referenceFile).getFeatures(frameLength)

	# Instantiate alignment computer
	diffs = []
	for c, t in enumerate(range(1,10)):
		onlineSmithWaterman = OnlineAlignment(reference, penalty, reward, baseScore)
		# Get matched regions for interval in sample
		startTime = sampleTimes[1]-t
		diff = []
		frameSample = convert.timeIntervalToFrame((startTime,sampleTimes[1]),frameLength)
		sampleInterval = sample[:,frameSample[0]:frameSample[1]]
		allMatchedRegions = onlineSmithWaterman.getNMostLikelyRegions(sampleInterval, n=1)
		if allMatchedRegions:
			resultEndTime = convert.frameToTime(allMatchedRegions[0][2], frameLength)
			diffs.append((c+1,abs(expectedTimes[1]-resultEndTime)))

	allDiffs.append(diffs)
	print allDiffs

with open(resultFile,'wb') as resultPickle:
	pickle.dump(allDiffs, resultPickle)
	resultPickle.close()




chromaFactory.persistCache()