import numpy as np
from ChromaFeatures import ChromaFactory
from ChromaFeatures import ChromaFeatures
from OnlineAlignment import OnlineAlignment
from Convert import Convert
from tabulate import tabulate

import sys



def referenceToSample(time,tempo):
	# from reference time to time on sample
	return np.float(time)/np.float(tempo)

def sampleToReference(time, tempo):
	# from sample time to time on reference
	return np.float(time)*np.float(tempo)

namePattern = "../samples/tempo/mond_%s.wav"
tempos = np.arange(0.5,1.6,0.1)
tempos = [0.8,1.0]

# Parameters
frameLength = 4*4096
penalty = 10
reward = 1
baseScore = 3


# Initialize services
chromaFactory = ChromaFactory()
convert = Convert()

# Map from tempos to files
tFrame = convert.frameLengthToTime(frameLength)
print "Frame Length: " + str(tFrame)
samples = {}
for t in tempos:
	# Weird rounding errors occur when using numpy floats as keys
	samples[str(t)] = namePattern % str(t).replace(".","")

refIndex = "1.0"
reference = chromaFactory.getChromaFeatures(samples[refIndex]).getFeatures(frameLength)
#reference = reference[:,0:40]

tRef = 5
fRef = convert.timeToFrame(tRef, frameLength)
tExp = tRef + convert.frameToTime((np.ceil(fRef) - fRef), frameLength)
fRef = np.ceil(fRef)

maxSamples = 10
print "Reference file: " + samples[refIndex]

for t in tempos:
	diff = []
	tab = [["n","tSample", "fMatch","fRef","fDiff","tMatch","tExp","tDiff"]]
	sampleName = samples[str(t)]
	print "Results for tempo: " + str(t) + " (file: " + sampleName + ")"

	# Compute sample features
	sample = chromaFactory.getChromaFeatures(sampleName).getFeatures(frameLength)
	for nrSteps in range(1,maxSamples,1):

		onlineSmithWaterman = OnlineAlignment(reference, penalty, reward, baseScore)
		fEnd = referenceToSample(fRef, t)
		fUp = np.ceil(fEnd)
		fDiff = fUp - fEnd
		tDiff  = fDiff * tFrame

		match = []
		for s in range(1,nrSteps+1):
			fStart = fUp - s
			match = onlineSmithWaterman.getNMostLikelyRegions(sample[:, fStart:fUp], n = 5)


		fMatch = match[0][2]
		tMatch = convert.frameToTime(fMatch, frameLength)
		tSample = convert.frameToTime(nrSteps, frameLength)
		diff.append([nrSteps,tSample,fMatch, fRef,abs(fRef-fMatch),tMatch,tExp, abs(tExp-tMatch)])
		tab.append(["{:10.4f}".format(x) for x in diff[-1]])

	print tabulate(tab)

chromaFactory.persistCache()