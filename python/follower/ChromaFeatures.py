import sys
sys.path.append("..")
import logging
import cPickle as pickle

from pymir import AudioFile
import numpy as np

class ChromaFactory:
	""" Factory for chroma features: Caches already computed features """

	cacheFile = "../metadata/featureCache.p"

	# Cache for chroma features
	chromaCache = {}

	def getChromaFeatures(self, wavFile, sampleRate = 44100):
		if not ChromaFactory.chromaCache:
			try:
				with open(ChromaFactory.cacheFile,'rb') as cacheFile:
					ChromaFactory.chromaCache = pickle.load(cacheFile)
					cacheFile.close()
			except IOError:
					print "No cachefile found."
			except EOFError:
					print "Invalid cache"

		if wavFile in ChromaFactory.chromaCache:
			#print "Return cached features for " + wavFile  
			return ChromaFactory.chromaCache[wavFile]
		else:
			chromaFeatures = ChromaFeatures(wavFile, sampleRate)
			ChromaFactory.chromaCache[wavFile] = chromaFeatures
			print "Computing new features for " + wavFile  
			return chromaFeatures

	def persistCache(self):
		print "Persisting Cache"
		with open(ChromaFactory.cacheFile,'wb') as cacheFile:
			pickle.dump(ChromaFactory.chromaCache, cacheFile, protocol = 2)
			cacheFile.close()

	def clearCache(self):
		print "Clearing Cache"
		ChromaFactory.chromaCache = {}

class ChromaFeatures:
	""" Contains methods to extract features from audio-samples """

	def __init__(self, wavFile, sampleRate = 44100):
		self.wavFile = wavFile
		self.sampleRate = np.float(sampleRate)
		self.cachedFeatures = {}

	def __computeChroma(self,frameLength):
		audiofile = AudioFile.open(self.wavFile)
		#windowFunction = np.hamming
		fixedFrames = audiofile.frames(frameLength)    
		spectra = [frame.spectrum() for frame in fixedFrames]    
		chroma = np.array([spectrum.chroma() for spectrum in spectra]).transpose()
		return chroma

	def getFeatures(self, frameLength):
		if (not frameLength in self.cachedFeatures):
			# Look if features are in cache
			self.cachedFeatures[frameLength] = self.__computeChroma(frameLength)
		
		return self.cachedFeatures[frameLength]