import numpy as np
from ChromaFeatures import ChromaFactory
from ChromaFeatures import ChromaFeatures
from OnlineAlignment import OnlineAlignment
from Convert import Convert
from tabulate import tabulate
from matplotlib import pyplot as plt

def plotChroma(chroma1, chroma2):
	fig = plt.figure()
	ax1 = fig.add_subplot(211)
	ax1.pcolor(chroma1,vmin=0, vmax=1)
	ax2 = fig.add_subplot(212)
	ax2.pcolor(chroma2,vmin=0,vmax=1)
	plt.show()

frameLength =4096

orch_file = "../samples/report/beet5_orch.wav"
piano_file = "../samples/report/beet5_piano.wav"

chromaFactory = ChromaFactory()

piano = chromaFactory.getChromaFeatures(piano_file).getFeatures(frameLength)
orch = chromaFactory.getChromaFeatures(orch_file).getFeatures(frameLength)

plotChroma(piano, orch)
