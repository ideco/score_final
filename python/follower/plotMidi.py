from pylab import *
import pickle

# Get data from file and convert for boxplot
rawdata = pickle.load( open( "../metadata/midi.p", "rb" ) )
plotData = []
for i in range(1, 11):
	plotData.append(rawdata[str(i)])


# Plot
fig, ax = subplots()

# Basic plot
bp = ax.boxplot(plotData,0,'',patch_artist=True)

# Axes
ax.set_xlabel('Tracked time [s]')
ax.set_ylabel('Error [s]')
plt.xticks(range(1,11), [ "{:10.2f}".format(x*rawdata['tFrame']) for x in range(1,11)])
plt.yticks(range(-60,121,20), [ "{:10.2f}".format(x*rawdata['tFrame']) for x in range(-60,120,20)])

#Style
## change outline color, fill color and linewidth of the boxes
for box in bp['boxes']:
    # change outline color
    box.set(linewidth=1)
    # change fill color
    box.set( facecolor = '#485A2C' )

## change color and linewidth of the whiskers
for whisker in bp['whiskers']:
    whisker.set(color='#485A2C', linewidth=2)

## change color and linewidth of the caps
for cap in bp['caps']:
    cap.set(linewidth=2)

## change color and linewidth of the medians
for median in bp['medians']:
    median.set(color='#FF0000', linewidth=2)

## change the style of fliers and their fill
for flier in bp['fliers']:
    flier.set(marker='o', alpha=0.5)

#tFrame = rawdata[tempo][nrSteps]['tFrame']
ax.set_xlabel('Tracked time [s]',fontsize=30)
ax.set_ylabel('Error [s]',fontsize=30)

#Style
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize(24) 

for tick in ax.yaxis.get_major_ticks():
	tick.label.set_fontsize(24)

ax.grid()

show()