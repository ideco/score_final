from ChromaFeatures import ChromaFeatures
from ChromaFeatures import ChromaFactory
from Alignment import Alignment
from Convert import Convert
from Evaluation import Evaluation
from PlotResults import *


baseFrameLength = 1024
penalty = 4
reward = 1
baseScore = 4
mappingFile = "../metadata/mappings.csv"
resultFile = "../metadata/results.csv"

# Create service instances
convert = Convert()
evaluation = Evaluation(mappingFile, resultFile)
chromaFactory = ChromaFactory()

for frameFactor in [4,8,16,32,64]:
	frameLength = frameFactor * baseFrameLength
	for penalty in range(2,3):
		for baseScore in range(4,8):
			for mapping in evaluation.mappings:
				# Retrieve file names
				sampleFile = mapping["sampleFile"]
				referenceFile = mapping["referenceFile"]

				# Retrieve mapped times
				sampleTimes = (mapping["sampleStart"], mapping["sampleEnd"])
				expectedTimes = (mapping["expectedStart"], mapping["expectedEnd"])

				# Get Chroma Features
				sample = chromaFactory.getChromaFeatures(sampleFile).getFeatures(frameLength)
				reference = chromaFactory.getChromaFeatures(referenceFile).getFeatures(frameLength)

				# Instantiate alignment computer
				smithWaterman = Alignment(reference, penalty, reward, baseScore)

				# Get matched regions for interval in sample
				frameSample = convert.timeIntervalToFrame(sampleTimes,frameLength)
				sampleInterval = sample[:,frameSample[0]:frameSample[1]]
				allMatchedRegions = smithWaterman.getNMostLikelyRegions(sampleInterval, n=1)

				for region in allMatchedRegions:
					pathScore = region["pathscore"]
					resultTime = convert.frameIntervalToTime(region['frameInterval'],frameLength)

					evaluation.addResult(resultTime, pathScore, mapping, frameLength, penalty, baseScore, reward)

chromaFactory.persistCache()
evaluation.writeResultsToCsv()

allResults, resultHeader = evaluation.readCSV(resultFile)
plotDifferentSamples(allResults)