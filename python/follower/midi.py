import numpy as np
from ChromaFeatures import ChromaFactory
from ChromaFeatures import ChromaFeatures
from OnlineAlignment import OnlineAlignment
from Evaluation import Evaluation
from Convert import Convert
import cPickle as pickle

# Parameters
minSteps = 1
maxSteps = 10

# Alignment Parameters
sampleRate = 44100
frameLength =4*4096
penalty = 4
reward = 1
baseScore = 4
mappingFile = "../metadata/mappings.csv"

# Initialize services
chromaFactory = ChromaFactory()
evaluation = Evaluation(mappingFile, "")
convert = Convert(sampleRate=sampleRate)

# Determine and print frame length
tFrame = convert.frameLengthToTime(frameLength)
print "Frame Length: " + str(tFrame)

results = {}
for nrSteps in range(minSteps, maxSteps+1):
	diffs = []
	for mapping in evaluation.mappings:
		
		# Retrieve file names
		midiFile = mapping["referenceFile"]
		sampleFile = mapping["sampleFile"]

		# Get chroma features
		midi = chromaFactory.getChromaFeatures(midiFile,sampleRate=sampleRate).getFeatures(frameLength)
		sample = chromaFactory.getChromaFeatures(sampleFile,sampleRate=sampleRate).getFeatures(frameLength)

		# Retrieve mapped times
		tExp = mapping["expectedEnd"]
		tSamp = mapping["sampleEnd"]
		
		# Convert times to full frames
		fExp = np.ceil(convert.timeToFrame(tExp,frameLength))
		fSamp = np.ceil(convert.timeToFrame(tSamp, frameLength))
		
		# Initialize alignment computer
		onlineSmithWaterman = OnlineAlignment(midi, penalty, reward, baseScore)

		# Simulate incoming audio stream
		fStart = fSamp - nrSteps
		match = onlineSmithWaterman.getNMostLikelyRegions(sample[:, fStart:fSamp], n = 5)

		fMatch = match[0][2]
		diffs.append(fMatch - fExp)

	# Add all diffs for one step number to one dictionary entry
	results[str(nrSteps)] = diffs

results['tFrame'] = tFrame
pickle.dump(results, open( "../metadata/midi.p", "wb" ) )
