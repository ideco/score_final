import numpy as np

class Convert:
	""" Provides methods to convert  between time and frame """

	def __init__(self, sampleRate = 44100):
		self.sampleRate = np.float(sampleRate)

	# Basic unit conversions
	def frameToTime(self, frame, frameLength):
		frame = np.float(frame)
		frameLength = np.float(frameLength)
		return (frame*frameLength)/self.sampleRate

	def timeToFrame(self, time, frameLength):
		time = np.float(time)
		frameLength = np.float(frameLength)
		return (time*self.sampleRate)/frameLength

	def frameLengthToTime(self, frameLength):
		return np.float(frameLength)/self.sampleRate

	# Complex conversions
	def frameIntervalToTime(self, frameInt, frameLength):
		start = self.frameToTime(frameInt[0],frameLength)
		end = self.frameToTime(frameInt[-1],frameLength)
		return (start, end)

	def timeIntervalToFrame(self, timeInt, frameLength):
		start = self.timeToFrame(timeInt[0], frameLength)
		end = self.timeToFrame(timeInt[1], frameLength)
		return (start, end)

