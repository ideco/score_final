#!/usr/bin/python

#
# Example boxplot code
#

from pylab import *
import pickle



def timestamp(x, pos):
	return '%1.3f' % (x*0.1857)

rawdata = pickle.load( open( "../metadata/tempos.p", "rb" ) )
#print rawdata

plotData = []
sortedTempos = []
steps = range(1,30)
for tempo in rawdata:
	if tempo == 'tFrame':
		continue
	sortedTempos.append(tempo)
	for nrSteps in rawdata[tempo]:
		error = abs(rawdata[tempo][nrSteps]['tDiff'])
		plotData.append((int(nrSteps),error))

data = []
for i in steps:
	res = []
	for tup in plotData:
		if tup[0] == i:
			res.append(tup[1])
	data.append((i,(sum(res)/len(res))))


formatter = FuncFormatter(timestamp)
fig, ax = plt.subplots()
ax.xaxis.set_major_formatter(formatter)

plot(*zip(*data))

sortedTempos = sorted(sortedTempos)

#tFrame = rawdata[tempo][nrSteps]['tFrame']
ax.set_xlabel('Tracked time [s]',fontsize=30)
ax.set_ylabel('Avg. Error [s]',fontsize=30)

#Style
for tick in ax.xaxis.get_major_ticks():
	tick.label.set_fontsize(24) 

for tick in ax.yaxis.get_major_ticks():
	tick.label.set_fontsize(24)

show()

