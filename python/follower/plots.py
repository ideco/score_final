from Evaluation import Evaluation
from Convert import Convert
from PlotResults import *

resultFile = "../metadata/results.csv"

# Create service instances
eva = Evaluation("../metadata/mappings.csv", 0)
convert = Convert()

allResults, resultHeader = eva.readCSV(resultFile)

def normalizedPathScore(result):
	time = result["resultEnd"] - result["resultStart"]
	frames = convert.timeToFrame(time, result["frameLength"])
	return result["pathScore"]/frames if frames != 0 else 0


for result in allResults:
	result["normalizedPathScore"] = normalizedPathScore(result)

def filterResults(allResults, feature, value):
	filteredResults = []
	for result in allResults:
		if result[feature] == value:
			filteredResults.append(result)
	return filteredResults

for frameLength in [1024,2048,4096,2*4096, 4*4096]:
	resultSets = []
	for penalty in range(1,4):
		for baseScore in range(1,4):
			filteredResults = filterResults(allResults, "frameLength", frameLength)
			filteredResults = filterResults(filteredResults, "penalty",penalty)
			filteredResults = filterResults(filteredResults, "baseScore",baseScore)
			resultSets.append(filteredResults)

	plotMultipleResults(resultSets, 3, 3, frameLength, saveFig=True)


