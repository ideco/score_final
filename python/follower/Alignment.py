import numpy as np
from scipy.spatial import distance as d

class Alignment:
	# Constants
	operations = {
		"nop":-1,
		"delete":1,
		"insert":2,
		"match":3
		}

	# Constructor
	def __init__(self, referenceFeatures, penalty, reward, baseScore):
		self.referenceFeatures = referenceFeatures
		self.penalty = penalty
		self.reward = reward
		self.baseScore = baseScore


	def __scoreFunction(self,v1,v2):
		# Note: Threshold has no apparent influence on results
		cosineSimilarity = np.abs(1 - d.cosine(v1,v2))
		return self.baseScore + cosineSimilarity if cosineSimilarity >= 0.95 else -1

	def __computePathMatrix(self, sampleFeatures, n=1):
		nrOfSamples = len(sampleFeatures[0,:])
		nrOfReferences = len(self.referenceFeatures[0,:])

		allFoundMax = []
		globalMax = 0

		# Initialize Matrices
		simMatrix = self.__initializeMatrix(nrOfSamples, nrOfReferences)
		pathMatrix = self.__initializeMatrix(nrOfSamples, nrOfReferences)

		# Smith Waterman
		for i in range(1,nrOfSamples):
			for j in range(1,nrOfReferences):
				match = self.reward *(simMatrix[i-1,j-1] + self.__scoreFunction(sampleFeatures[:,i-1], self.referenceFeatures[:,j-1]))
				delete = simMatrix[i-1,j] - self.penalty
				insert = simMatrix[i, j-1] - self.penalty
				maxScore = max(0,match,delete,insert)

				# Fill in Matrix for Backtracking            
				simMatrix[i,j] = maxScore
				if maxScore == match:
					pathMatrix[i,j] = 3
				elif maxScore == insert:
					pathMatrix[i,j] = 2
				elif maxScore == delete:
					pathMatrix[i,j] = 1
				elif maxScore == 0:
					pathMatrix[i,j] = -1
				else:
					print "error"
	   
				if  maxScore > globalMax:
					if allFoundMax and i - allFoundMax[0][1] <= 5 and j - allFoundMax[0][2] <= 5:
						allFoundMax.pop(0)
						if (len(allFoundMax) >= n):
							allFoundMax.pop()
					allFoundMax.insert(0,(maxScore,i,j))                

					globalMax = maxScore

		return pathMatrix, allFoundMax

	def __backTracking(self, pathMatrix, allFoundMax):
		allMatchedRegions = []   # Container for all Results
		for localMax in allFoundMax:
			matchedRegion = []   # Local Result
			i, j = localMax[1], localMax[2]
			while pathMatrix[i,j] > 0:
				if pathMatrix[i,j] == 3:
					i -= 1
					j -= 1
					matchedRegion.insert(0, j)
				elif pathMatrix[i,j] == 2:
					j -= 1
				elif pathMatrix[i,j] == 1:
					i -= 1
			allMatchedRegions.append({"frameInterval":(matchedRegion[0], matchedRegion[-1]),
											"pathscore":localMax[0]})
		return allMatchedRegions


	def __initializeMatrix(self, nrOfSamples, nrOfReferences):
		return np.zeros((nrOfSamples+1,nrOfReferences+1),dtype=float)

	def getNMostLikelyRegions(self, sampleFeatures, n=1):
		pathMatrix, allFoundMax = self.__computePathMatrix(sampleFeatures, n = n)
		allMatchedRegions = self.__backTracking(pathMatrix, allFoundMax)
		return allMatchedRegions
