import csv
from scipy.interpolate import interp1d
import numpy as np

def getSamplePoints(fileToLoad):
	samplePoints = {}
	with open(fileToLoad, "rb") as csvFile:
		reader = csv.reader(csvFile, delimiter=";", quotechar="'")
		header = reader.next()
		for row in reader:
			if row[0] not in samplePoints:
				samplePoints[row[0]] = []
			
			givenPoint = float(row[2])
			expectedPoint = float(row[3])
			samplePoints[row[0]].append((givenPoint, expectedPoint))
	return samplePoints


def getExpectedPoint(csvFile,sampleFile, samplePoint):
	samplePoints = getSamplePoints(csvFile)[sampleFile]
	sample = []
	expected = []
	for pair in samplePoints:
		sample.append(pair[0])
		expected.append(pair[1])

	f = interp1d(sample,expected, kind="cubic")
	return f(samplePoint)