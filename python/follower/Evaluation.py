import csv

class Evaluation:
	""" Provides methods for evaluating the tracking algorithm """

	# Parsed mappings
	mappings = []
	headerMappings = []

	# Results
	results = []
	headerResults = []

	def __init__(self, csvMappings, resultFile):
		self.mappings, self.headerMappings = self.__loadCSV(csvMappings)
		self.resultFile = resultFile


	def __loadCSV(self, fileToLoad):
		tempMappings = []
		with open(fileToLoad, "rb") as csvFile:
			reader = csv.reader(csvFile, delimiter=";", quotechar="'")
			header = reader.next()
			for row in reader:
				data = {}
				for i, value in enumerate(row):
					try:
						data[header[i]] = float(value)
					except ValueError:
						data[header[i]] = value
				tempMappings.append(data)
		return tempMappings, header

	def addResult(self,
					resultTime, 
					pathScore, 
					mapping,
					frameLength,
					penalty, 
					baseScore, 
					reward):

		# Dictionary with result data
		result = {}

		# Add results
		result["resultStart"] = resultTime[0]
		result["resultEnd"] = resultTime[1]
		result["pathScore"] = pathScore
		result.update(mapping)
		result["frameLength"] = frameLength
		result["penalty"] = penalty
		result["baseScore"] = baseScore
		result["reward"] = reward
		result["score"] = self.getScore((mapping["expectedStart"], mapping["expectedEnd"]), 
									(resultTime[0], resultTime[1]))


		# Append to results
		self.results.append(result)

	def getScore(self,expected, result):
		score = max(0, min(expected[1], result[1]) - max(expected[0], result[0]))
		return score / (expected[1]-expected[0])

	def writeResultsToCsv(self):
		# Not very nice but seemst to be the only way to introduce name order
		self.headerResults += self.headerMappings
		self.headerResults.append("resultStart")
		self.headerResults.append("resultEnd")
		self.headerResults.append("pathScore")
		self.headerResults.append("frameLength")
		self.headerResults.append("penalty")
		self.headerResults.append("baseScore")
		self.headerResults.append("reward")
		self.headerResults.append("score")

		with open(self.resultFile, 'wb') as csvFile:
			dict_writer = csv.DictWriter(csvFile, self.headerResults, delimiter=";", quotechar="'")
			dict_writer.writer.writerow(self.headerResults)
			dict_writer.writerows(self.results)


	def readCSV(self,csvFile):
		return self.__loadCSV(csvFile)

