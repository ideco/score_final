import numpy as np
from matplotlib import pyplot as plt
import cPickle as pickle

resultFile = "../metadata/diffs.p"

with open(resultFile,'rb') as cacheFile:
	allDiffs = pickle.load(cacheFile)
	cacheFile.close()

print allDiffs

plt.figure()
for diffs in allDiffs:
	x = []
	y = []
	for diff in diffs:
		x.append(diff[0])
		y.append(diff[1])
		plt.plot(x,y)
plt.show()
