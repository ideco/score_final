from ChromaFeatures import ChromaFeatures
from ChromaFeatures import ChromaFactory

import numpy as np
from matplotlib import pyplot as plt
from scipy.spatial import distance as d
from scipy import interpolate

def scoreFunction(v1,v2):
	# Note: Threshold has no apparent influence on results
	cosineSimilarity = np.abs(1 - d.cosine(v1,v2))
	return cosineSimilarity 

frameLength = 4096
namePattern = "/home/ivo/dev/score_following/python/samples/noise/mond_%s.wav"

chromaFactory = ChromaFactory()

noiseLevels = [0,0.1, 0.5, 1, 2, 5, 8, 10]
suffixes = [str(l).replace(".","") for l in noiseLevels]

referenceFile = namePattern % suffixes[0]
reference = chromaFactory.getChromaFeatures(referenceFile).getFeatures(frameLength)

avgSims = []
for suf in suffixes:
	noiseFile = namePattern % suf

	noisy = chromaFactory.getChromaFeatures(noiseFile).getFeatures(frameLength)

	lenRef = len(reference[0,:])
	lenNoisy = len(noisy[0,:])


	sims = []
	for i in range(0,min(lenRef,lenNoisy)):
		v1 = reference[:,i]
		v2 = noisy[:,i]
		sims.append(scoreFunction(v1, v2))

	avgSims.append(sum(sims)/len(sims));
	print "Avg sim for noise level: " + suf + ": " + str(sum(sims)/len(sims))




tck = interpolate.splrep(noiseLevels, avgSims, s=0)
xnew = np.linspace(0,11,40)
ynew = interpolate.splev(xnew, tck, der=0)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
ax.plot(xnew,ynew, color='black')
ax.plot(noiseLevels, avgSims, "o", color='black')
plt.xlabel(r'$\frac{noise}{signal}$',fontsize=20,horizontalalignment='right')
plt.ylabel(r'$similarity$',fontsize=20)
plt.xlim(0,11)
plt.ylim(0.82,1.02)

almost_black = '#262626'
# Change the axis title to off-black
ax.title.set_color(almost_black)
ax.set_title('Avg. similarity of chroma features with added noise')

spines_to_remove = ['top', 'right']
for spine in spines_to_remove:
	ax.spines[spine].set_visible(False)

spines_to_keep = ['bottom', 'left']
for spine in spines_to_keep:
    ax.spines[spine].set_linewidth(0.5)
    ax.spines[spine].set_color(almost_black)

ax.xaxis.set_ticks_position('none')
ax.yaxis.set_ticks_position('none')

#ax.set_xscale('log')
plt.show()

chromaFactory.persistCache()