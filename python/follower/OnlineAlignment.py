import numpy as np
from scipy.spatial import distance as d
from collections import deque

class OnlineAlignment:
	# Constants
	operations = {
		"nop":-1,
		"delete":1,
		"insert":2,
		"match":3
		}

	# Constructor
	def __init__(self, referenceFeatures, penalty, reward, baseScore):
		self.referenceFeatures = referenceFeatures
		self.penalty = np.float(penalty)
		self.reward = np.float(reward)
		self.baseScore = np.float(baseScore)
		self.previousRow = None


	def __scoreFunction(self,v1,v2):
		# Note: Threshold has no apparent influence on results
		cosineSimilarity = np.abs(1 - d.cosine(v1,v2))
		return (self.baseScore + self.reward*cosineSimilarity)  if cosineSimilarity >= 0.95 else -1

	# TODO: Normalitze score with number of samples
	def __computePathMatrix(self, sampleFeatures, n=1):
		nrOfSamples = len(sampleFeatures[0,:])
		nrOfReferences = len(self.referenceFeatures[0,:])

		allFoundMax = []
		globalMax = 0

		# Initialize Matrices
		simMatrix = self.__initializeMatrix(nrOfSamples, nrOfReferences)
		pathMatrix = self.__initializeMatrix(nrOfSamples, nrOfReferences)

		if self.previousRow is not None:
			simMatrix[0,:] = self.previousRow
		# Smith Waterman
		for i in range(1,nrOfSamples+1):
			for j in range(1,nrOfReferences+1):
				match = simMatrix[i-1,j-1] + self.__scoreFunction(sampleFeatures[:,i-1], self.referenceFeatures[:,j-1])
				delete = simMatrix[i-1,j] - self.penalty
				insert = simMatrix[i, j-1] - self.penalty
				maxScore = max(0,match,delete,insert)

				# Fill in Matrix for Backtracking            
				simMatrix[i,j] = maxScore
				if maxScore == match:
					pathMatrix[i,j] = 3
				elif maxScore == insert:
					pathMatrix[i,j] = 2
				elif maxScore == delete:
					pathMatrix[i,j] = 1
				elif maxScore == 0:
					pathMatrix[i,j] = -1
				else:
					print "error"

				# TODO: Don't store all elements
				if  maxScore >= globalMax:
					allFoundMax.insert(0,((maxScore,i,j)))                
					globalMax = maxScore

		self.previousRow = [x for x in simMatrix[-1,:]]
		#print simMatrix
		return pathMatrix, allFoundMax[0:n]

	def __initializeMatrix(self, nrOfSamples, nrOfReferences):
		return np.zeros((nrOfSamples+1,nrOfReferences+1),dtype=float)

	def getNMostLikelyRegions(self, sampleFeatures, n=1):
		pathMatrix, allFoundMax = self.__computePathMatrix(sampleFeatures, n = n)
		return allFoundMax
