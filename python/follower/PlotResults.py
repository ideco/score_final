import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm

def plotMultipleResults(resultSets,nrPenalty,nrScore, frameLength, saveFig = False):
	fig = plt.figure()
	fig.set_size_inches(18.5,10.5)
	for n, resultSet in enumerate(resultSets):
		ax = fig.add_subplot(nrPenalty,nrScore,n+1)
		plotDifferentSamples(resultSet,fig,ax)
	if saveFig:
		plt.savefig("../figures/" + str(frameLength) + ".png", dpi = 100)
	else:
		plt.show()

def plotDifferentSamples(allResults,f=None,ax=None):
	if f is None or ax is None:
		f, ax = plt.subplots()
	
	samples = {}
	maxPath = 0
	i = 0

	for result in allResults:
		sampleInterval = str((result["sampleStart"], result["sampleEnd"]))
		if sampleInterval not in samples:
			samples[sampleInterval] = i
			i += 1
		maxPath = result["normalizedPathScore"] if result["normalizedPathScore"] > maxPath else maxPath

	colors = cm.rainbow(np.linspace(0, 1, i))

	for result in allResults:
		sampleInterval = str((result["sampleStart"], result["sampleEnd"]))
		c = colors[samples[sampleInterval]]
		x = result["expectedEnd"]
		y = result["resultEnd"] 
		area = 10*((np.pi * (result['normalizedPathScore'])**2) / np.float(maxPath)) # 0 to 15 point radiuses
		ax.scatter(x,y,s=area, alpha=0.8, color=c)

	   
	# Format axes   
	plt.xlabel('Expected End-Time')
	plt.ylabel('Matched End-Time')
	maxValue = max(ax.get_xlim()[1], ax.get_ylim()[1])
	plt.xlim(xmin=0, xmax=maxValue)
	plt.ylim(ymin=0, ymax=maxValue)
	ax.set_aspect('equal')
	ax.plot(ax.get_xlim(), ax.get_ylim(), ls="--", c=".3")
	#desc = "Frame length = " + str(result["frameLength"]) + "\npenalty = " + str(result["penalty"]) + "\nbaseScore = " + str(result["baseScore"])
	#ax.annotate(desc, xy=(1, 0), xycoords='axes fraction', fontsize=8,
    #    xytext=(-5, 5), textcoords='offset points',
    #    ha='right', va='bottom')
