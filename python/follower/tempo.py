import numpy as np
from ChromaFeatures import ChromaFactory
from ChromaFeatures import ChromaFeatures
from OnlineAlignment import OnlineAlignment
from Convert import Convert
from tabulate import tabulate

import csv
import sys

import pickle

def referenceToSample(time,tempo):
	# from reference time to time on sample
	return np.float(time)/np.float(tempo)

def sampleToReference(time, tempo):
	# from sample time to time on reference
	return np.float(time)*np.float(tempo)

namePattern = "../samples/temnoise/op63_2_%s.wav"
refName = '../samples/temnoise/ref.wav'
tempos = np.arange(0.5,1.6,0.1)
tempos = [0.5, 0.8, 1.0, 1.2, 1.5]

# Parameters
sampleRate = 44100
frameLength =2*4096
penalty = 2
reward = 1
baseScore = 1

# Initialize services
chromaFactory = ChromaFactory()
convert = Convert(sampleRate=sampleRate)

# Map from tempos to files
tFrame = convert.frameLengthToTime(frameLength)
print "Frame Length: " + str(tFrame)
samples = {}
for t in tempos:
	# Weird rounding errors occur when using numpy floats as keys
	samples[str(t)] = namePattern % str(t).replace(".","")

refIndex = "1.0"
reference = chromaFactory.getChromaFeatures(refName,sampleRate=sampleRate).getFeatures(frameLength)
#reference = reference[:,0:40]

tRef = 35
fRef = convert.timeToFrame(tRef, frameLength)
tExp = tRef + convert.frameToTime((np.ceil(fRef) - fRef), frameLength)
fRef = np.ceil(fRef)

minSamples = 1
maxSamples = 30
print "Reference file: " + samples[refIndex]

diffs = {}
for t in tempos:
	dicDiff = {}
	diff = []

	tab = [["n","tempo","tFrame","tSample", "fMatch","fRef","fDiff","tOnSample","tMatch","tExp","tDiff"]]
	sampleName = samples[str(t)]
	print "Results for tempo: " + str(t) + " (file: " + sampleName + ")"
	
	# Compute sample features
	sample = chromaFactory.getChromaFeatures(sampleName,sampleRate=sampleRate).getFeatures(frameLength)
	for nrSteps in range(minSamples,maxSamples,1):
		onlineSmithWaterman = OnlineAlignment(reference, penalty, reward, baseScore)
		fEnd = referenceToSample(fRef, t)
		fUp = np.ceil(fEnd)
		fDiff = fUp - fEnd
		tDiff  = fDiff * tFrame

		tOnSample = convert.frameToTime(fEnd,frameLength)

		match = []
		fStart = fUp - nrSteps
		match = onlineSmithWaterman.getNMostLikelyRegions(sample[:, fStart:fUp], n = 5)
		
		fMatch = match[0][2]
		tMatch = convert.frameToTime(fMatch, frameLength)
		tSample = convert.frameToTime(nrSteps, frameLength)
		data = [nrSteps,t,tFrame,tSample,fMatch, fRef,abs(fRef-fMatch),tOnSample,tMatch,tExp, tExp-tMatch]
		dicData = {}
		for i, key in enumerate(tab[0]):
			dicData[key] = data[i]

		dicDiff[str(nrSteps)] = dicData
		diff.append(data)

		tab.append(["{:10.4f}".format(x) for x in diff[-1]])
	diffs[str(t)] = dicDiff 
	print tabulate(tab)

diffs['tFrame'] = tFrame
pickle.dump(diffs, open( "../metadata/tempos.p", "wb" ) )
chromaFactory.persistCache()